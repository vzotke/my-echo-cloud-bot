# Simple echo Telegramm bot

Bot name `@mySimpleEcho_bot`

Features:
* Get user message
* Responds with the same message but in upper case
* Saves conversation history (and exposes it over REST)

Created for learning reason.
