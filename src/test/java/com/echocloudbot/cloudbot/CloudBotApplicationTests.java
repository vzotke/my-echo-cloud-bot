package com.echocloudbot.cloudbot;

import com.echocloudbot.cloudbot.rest.controller.HistoryController;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest
class CloudBotApplicationTests {

    @Autowired
    HistoryController historyController;

    @SneakyThrows
    @Test
    void contextLoads() {
        assertThat(historyController).isNotNull();
    }
}
