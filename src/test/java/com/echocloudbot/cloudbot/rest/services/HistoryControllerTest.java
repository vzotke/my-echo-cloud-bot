package com.echocloudbot.cloudbot.rest.services;

import com.echocloudbot.cloudbot.rest.controller.HistoryController;
import com.echocloudbot.cloudbot.rest.models.HistoryEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HistoryControllerTest {

    List<HistoryEntity> historyEntityList = new ArrayList<>() {{
        add(new HistoryEntity("4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b", 1, new Date(1618932628405L), "Test message"));
        add(new HistoryEntity("bca0f108-4607-40f8-8ac7-6d09f237b0af", 1, new Date(1618932628409L), "Second msg"));
    }};

    HistoryEntity historyEntity = new HistoryEntity("4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b",
            1,
            new Date(1618932628405L),
            "Test message");

    @InjectMocks
    HistoryController historyController;

    @Mock
    HistoryService historyService;

    @Test
    void testGetHistory() {
        when(historyService.getAllHistory(any(Long.class))).thenReturn(historyEntityList);
        List<HistoryEntity> response = historyController.getHistory(111);
        assertEquals(historyEntityList, response);
    }

    @Test
    void testGetSingleMessageHistory() {
        when(historyService.getSingleMessageHistory(any(String.class))).thenReturn(historyEntity);
        HistoryEntity response = historyController.getSingleMessageHistory("4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b");
        assertEquals(historyEntity, response);
    }

    @Test
    void testSaveHistory() throws ExecutionException, InterruptedException {
        when(historyService.saveHistory(any(HistoryEntity.class))).thenReturn("16 April 2021 at 18:14:16 UTC+3");
        String result = historyController.createHistory(historyEntity);
        assertEquals("16 April 2021 at 18:14:16 UTC+3", result);
    }

    @Test
    void testUpdateHistory() {
        final String UPDATED_MESSAGE = "Document with UUID 4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b has been deleted";
        when(historyService.updateHistory(any(Long.class), any(String.class), any(HistoryEntity.class)))
                .thenReturn(UPDATED_MESSAGE);
        String result = historyController.updateHistory(1,
                "4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b",
                historyEntity);
        assertEquals(UPDATED_MESSAGE, result);
    }

    @Test
    void testDeleteHistory() {
        final String DELETED_MESSAGE = "Document with UUID 4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b has been deleted";
        when(historyService.deleteHistory(any(Long.class),any(String.class))).thenReturn(DELETED_MESSAGE);
        String result = historyController.deleteHistory(1, "4247fd1a-88ce-41c2-af4c-b7ee4fe8bc2b");
        assertEquals(DELETED_MESSAGE, result);
    }

}
