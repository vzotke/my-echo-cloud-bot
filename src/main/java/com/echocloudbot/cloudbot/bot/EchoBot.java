package com.echocloudbot.cloudbot.bot;

import com.echocloudbot.cloudbot.rest.models.HistoryEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Locale;

@Log4j2
public class EchoBot extends TelegramLongPollingBot {

    String serviceUrl = "http://localhost:8080/";


    @Override
    public String getBotUsername() {
        return "mySimpleEcho_bot";
    }

    @Override
    public String getBotToken() {
        return "1750194281:AAGOIuWg81RugUfAYQTRQ11-CW_H8aNRgaE";
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            long chatId = update.getMessage().getChatId();

            SendMessage message = new SendMessage();
            message.setChatId(String.valueOf(chatId));

            String answerMessage = messageText.toUpperCase(Locale.ROOT);
            message.setText(answerMessage);
            saveHistory(chatId, messageText);
            saveHistory(chatId, answerMessage);
            try {
                execute(message);
            } catch (TelegramApiException e) {
                log.error(e.getMessage());
            }
        }
    }

    private void saveHistory(long chatId, String message) {
        RestTemplate restTemplate = new RestTemplate();
        String url = serviceUrl + "log?chatId=" + chatId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HistoryEntity history = new HistoryEntity(chatId, message);
        ObjectMapper objectMapper = new ObjectMapper();
        String historyJSON = null;
        try {
            historyJSON = objectMapper.writeValueAsString(history);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        HttpEntity<String> entity = new HttpEntity<>(historyJSON, headers);
        restTemplate.postForObject(url, entity, String.class);
    }
}
