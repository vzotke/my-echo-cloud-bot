package com.echocloudbot.cloudbot.rest.services;

import com.echocloudbot.cloudbot.rest.exceptions.NotFoundException;
import com.echocloudbot.cloudbot.rest.models.HistoryEntity;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

@Service
@Log4j2
public class HistoryService {

    public static final String COL_NAME = "logs";
    private static final String INTERRUPTED_EXCEPTION = "INTERRUPTED EXCEPTION!";

    public String saveHistory(HistoryEntity historyEntity) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document().set(historyEntity);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

    public List<HistoryEntity> getAllHistory(long chatId) {

        List<HistoryEntity> historyList = new ArrayList<>();

        Firestore dbFirestore = FirestoreClient.getFirestore();
        Iterable<DocumentReference> collections =
                dbFirestore.collection("logs").listDocuments();
        collections.forEach(x -> {
            try {
                HistoryEntity historyEntity = x.get().get().toObject(HistoryEntity.class);
                if (Objects.requireNonNull(historyEntity).getChatId() == chatId) {
                    historyList.add(historyEntity);
                }
            } catch (ExecutionException | InterruptedException e) {
                log.error(INTERRUPTED_EXCEPTION, e);
                Thread.currentThread().interrupt();
            }
        });
        historyList.forEach(log::debug);
        return historyList;
    }

    public String updateHistory(long chatId, String uuid, HistoryEntity updateHistoryEntity) {

        Firestore dbFirestore = FirestoreClient.getFirestore();
        Iterable<DocumentReference> collections =
                dbFirestore.collection("logs").listDocuments();
        collections.forEach(x -> {
            try {
                HistoryEntity historyEntity = x.get().get().toObject(HistoryEntity.class);
                if (historyEntity != null && historyEntity.getChatId() == chatId
                        && historyEntity.getUuid().equals(uuid)) {
                    dbFirestore.collection(COL_NAME).document(x.get().get().getId()).set(updateHistoryEntity);
                }
            } catch (ExecutionException | InterruptedException e) {
                log.error(INTERRUPTED_EXCEPTION, e);
                Thread.currentThread().interrupt();
            }
        });

        return "History with UUID:" + uuid + " has been updated";
    }

    public String deleteHistory(long chatId, String uuid) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        Iterable<DocumentReference> collections =
                dbFirestore.collection("logs").listDocuments();
        collections.forEach(x -> {
            try {
                HistoryEntity historyEntity = x.get().get().toObject(HistoryEntity.class);
                if (historyEntity != null && historyEntity.getChatId() == chatId
                        && historyEntity.getUuid().equals(uuid)) {
                    dbFirestore.collection(COL_NAME).document(x.get().get().getId()).delete();
                }
            } catch (ExecutionException | InterruptedException e) {
                log.error(INTERRUPTED_EXCEPTION, e);
                Thread.currentThread().interrupt();
            }
        });
        return "Document with UUID " + uuid + " has been deleted";
    }

    public HistoryEntity getSingleMessageHistory(String uuid) {

        Firestore dbFirestore = FirestoreClient.getFirestore();
        Iterable<DocumentReference> collections =
                dbFirestore.collection("logs").listDocuments();

        for (DocumentReference doc : collections) {
            try {
                HistoryEntity entity = Objects.requireNonNull(doc.get().get().toObject(HistoryEntity.class));
                if (entity.getUuid().equals(uuid)) {
                    return entity;
                }
            } catch (ExecutionException | InterruptedException e) {
                log.error(INTERRUPTED_EXCEPTION, e);
                Thread.currentThread().interrupt();
            }
        }
        throw new NotFoundException("Doc reference not found");
    }
}
