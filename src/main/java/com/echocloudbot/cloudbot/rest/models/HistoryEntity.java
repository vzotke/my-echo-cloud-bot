package com.echocloudbot.cloudbot.rest.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class HistoryEntity {

    public HistoryEntity() {
    }

    public HistoryEntity(long chatId, String message) {
        this.uuid = java.util.UUID.randomUUID().toString();
        this.chatId = chatId;
        this.recordTime = new Date();
        this.message = message;
    }

    public HistoryEntity(String uuid, long chatId, Date recordTime, String message) {
        this.uuid = uuid;
        this.chatId = chatId;
        this.recordTime = recordTime;
        this.message = message;
    }

    private String uuid;

    private long chatId;

    private Date recordTime;

    private String message;

}
