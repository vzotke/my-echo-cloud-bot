package com.echocloudbot.cloudbot.rest.controller;

import com.echocloudbot.cloudbot.rest.models.HistoryEntity;
import com.echocloudbot.cloudbot.rest.services.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("log")
public class HistoryController {

    @Autowired
    HistoryService historyService;

    @GetMapping
    public List<HistoryEntity> getHistory(@RequestParam long chatId) {
        return historyService.getAllHistory(chatId);
    }

    @GetMapping(value = "/{uuid}")
    public HistoryEntity getSingleMessageHistory(@PathVariable("uuid") String uuid) {
        return historyService.getSingleMessageHistory(uuid);
    }

    @PostMapping
    public String createHistory(@RequestBody HistoryEntity history) throws InterruptedException, ExecutionException {
        return historyService.saveHistory(history);
    }

    @PutMapping
    public String updateHistory(@RequestParam long chatId, @RequestParam String uuid,
            @RequestBody HistoryEntity history) {
        return historyService.updateHistory(chatId, uuid, history);
    }

    @DeleteMapping
    public String deleteHistory(@RequestParam long chatId, @RequestParam String uuid) {
        return historyService.deleteHistory(chatId, uuid);
    }
}
