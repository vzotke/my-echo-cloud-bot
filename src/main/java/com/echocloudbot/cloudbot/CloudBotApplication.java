package com.echocloudbot.cloudbot;

import com.echocloudbot.cloudbot.bot.EchoBot;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@SpringBootApplication
public class CloudBotApplication {

    public static void main(String[] args) {
        //Start Post Service
        SpringApplication.run(CloudBotApplication.class, args);

        //Start Telegramm bot
        ApiContextInitializer.init();
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            botsApi.registerBot(new EchoBot());
        } catch (
                TelegramApiException e) {
            e.printStackTrace();
        }
    }
}

